import re
import sys
import pickle
import random
import logging
import requests
import dateparser

from logger import Logger

class Browser(Logger, object):
    __UAGENT = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/55.0.2883.87 Chrome/55.0.2883.87 Safari/537.36'
    ]
    __ACCEPT_LANGUAGE = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'

    def __init__(self, **kwargs):
        super(Browser, self).__init__(logname=kwargs.get('logname'))

        self.cookies = None
        if kwargs.get('debug'):
            logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s][%(name)s] - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        self.session = requests.Session()
        self.log('initialize ... ')

    def user_agents(self):
        return random.choice(self.__UAGENT)

    def accept_language(self):
        return self.__ACCEPT_LANGUAGE

    def get_cookies(self):
        return pickle.dumps(self.session.cookies._cookies)

    def set_cookies(self, cookies=None):
        if cookies:
            self.cookies = cookies

        if isinstance(self.cookies, str) or isinstance(self.cookies, unicode):
            self.cookies = pickle.loads(self.cookies)

        try:
            self.verify_cookie()
            if isinstance(self.cookies, list):
                for cookie in self.cookies:
                    try:
                        self.session.cookies.set(**cookie)
                    except:
                        raise
            return True
        except:
            return False

    def verify_cookie(self):
        cookiejar = ['version', 'name', 'value', 'port', 'domain', 'path', 'secure', 'expires', 'discard', 'comment', 'comment_url']
        if isinstance(self.cookies, dict):
            self.cookies = [self.cookies]

        cookies = list()
        for cookie in self.cookies:
            allowed_cookie = {}
            for k,v in cookie.iteritems():
                if k in cookiejar:
                    if k == 'expires':
                        v = re.sub(r'^\w+,\s?', '', v.strip(), flags=re.U)
                        v = dateparser.parse(v)
                    allowed_cookie[k] = v
            cookies.append(allowed_cookie)
        self.cookies = cookies

    def set_proxies(self, proxies=None):
        if proxies and isinstance(proxies, dict):
            self.session.proxies.update(proxies)