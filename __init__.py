__author__ = 'Khoir MS'

import re
from browser import Browser
from endpoints import Endpoints

class GCse(Browser):

    def __init__(self, **kwargs):
        super(GCse, self).__init__(**kwargs)

        self.__apikey = ''
        self.__cse_id = ''
        self.is_credential_set = False
        self.response = None

    def set_cse_id(self, cse_id):
        self.log('setup cse id {} ...'.format(cse_id))
        self.__cse_id = cse_id

    def set_apikey(self, apikey):
        self.log('setup api key {} ...'.format(apikey))
        self.__apikey = apikey

    def search(self, query, max_result=10, sort='date', date_restrict='w', start=1, **kwargs):
        accept_date_restrict = ['h', 'd', 'w', 'm', 'y']

        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())
        if date_restrict and not date_restrict in accept_date_restrict:
            date_restrict = ''

        self.__is_credential_set()
        endpoint = Endpoints.search(query=query, apikey=self.__apikey, cse_id=self.__cse_id,
                                    max_result=max_result, sort=sort, date_restrict=date_restrict, start=start)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def __is_credential_set(self):
        if not (self.__apikey and self.__cse_id):
            raise Exception('please set credential key apikey and cse_id')

        self.is_credential_set = True

    def get_api_errors(self):
        eresponse = self.response.json()
        ecode = eresponse['error']['code']
        emessage = eresponse['error']['message']

        if ecode == 403:
            reason = eresponse['error']['errors'][0]['reason']
            if re.search(r'dailyLimitExceeded', reason, flags=re.I):
                raise DailyLimitExcedeed(emessage)

        if ecode != 200:
            raise BaseApiException(emessage)

class DailyLimitExcedeed(Exception):
    pass

class BaseApiException(Exception):
    pass

if __name__=="__main__":
    import json
    cse = GCse(debug=True)

    try:
        cse.set_cse_id(cse_id='014042265073539615289:a0hnaatpot8')
        cse.set_apikey(apikey='AIzaSyDeoMnSSgErFSyYa7WnYTLCLwAlJmxM10o')
        data = cse.search(query='dana haji')
        data.pop('url')

        print json.dumps(data, indent=4)
    except Exception as e:
        print e