__author__ = 'Khoir MS'

import urllib
import urlparse

class Endpoints:

    __BASE_URL = 'https://www.googleapis.com'
    __API_URL = 'https://www.googleapis.com/customsearch/v1'
    __CUSTOM_SEARCH = '{api_url}/?q={query}&num={count}&start={start}&cx={cse_id}&sort={sort}&dateRestrict={date_restrict}&key={apikey}'

    @staticmethod
    def search(query, cse_id, apikey, max_result=10, sort='date', date_restrict='', start=''):
        if max_result < 1 or max_result > 10: max_result = 10
        endpoint = Endpoints.__CUSTOM_SEARCH.format(api_url=Endpoints.__API_URL, query=query, cse_id=cse_id, apikey=apikey,
                                                    count=max_result, date_restrict=date_restrict, start=start, sort=sort)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def parse_qstring(url):
        return dict(urlparse.parse_qsl(urlparse.urlsplit(url).query))

if __name__=='__main__':
    print Endpoints.search(query="dede", cse_id='014042265073539615289:a0hnaatpot8', apikey='AIzaSyDeoMnSSgErFSyYa7WnYTLCLwAlJmxM10o')